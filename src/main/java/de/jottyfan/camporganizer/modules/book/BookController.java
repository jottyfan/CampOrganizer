package de.jottyfan.camporganizer.modules.book;

import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import de.jottyfan.camporganizer.Controller;
import de.jottyfan.camporganizer.profile.ProfileBean;

/**
 * 
 * @author jotty
 *
 */
@Named
@RequestScoped
public class BookController extends Controller {

	@Inject
	private ProfileBean profileBean;

	@Inject
	@Named("bookModel")
	private BookModel model;

	public String toBook() {
		FacesContext facesContext = FacesContext.getCurrentInstance();

		model.toBook(facesContext, profileBean);
		return "/pages/book.jsf";
	}

	public String toBookWithProfile() {
		FacesContext facesContext = FacesContext.getCurrentInstance();

		model.loadPersons(facesContext, profileBean);
		return toBook();
	}

	public String toBook(Integer campPk) {
		model.getBean().setFkCamp(campPk);
		return toBook();
	}

	public String doBookWellKnown() {
		FacesContext facesContext = FacesContext.getCurrentInstance();

		model.doBookWellKnown(facesContext, profileBean);
		return toBook();
	}
	
	public String doBook() {
		FacesContext facesContext = FacesContext.getCurrentInstance();

		model.doBook(facesContext, profileBean);
		if (profileBean.getIsEmpty()) {
			profileBean.setForename(null);
			profileBean.setSurname(null);
			profileBean.setUsername(null);
		}
		return toBook();
	}

	public void setProfileBean(ProfileBean profileBean) {
		this.profileBean = profileBean;
	}

	public void setModel(BookModel model) {
		this.model = model;
	}
}
