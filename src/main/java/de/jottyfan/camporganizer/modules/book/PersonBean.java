package de.jottyfan.camporganizer.modules.book;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvBindByPosition;
import com.opencsv.bean.CsvDate;

import de.jottyfan.camporganizer.CampBean;
import de.jottyfan.camporganizer.db.converter.EnumConverter;
import de.jottyfan.camporganizer.db.jooq.enums.EnumCamprole;
import de.jottyfan.camporganizer.db.jooq.enums.EnumSex;

/**
 * 
 * @author jotty
 *
 */
@Named
@SessionScoped
public class PersonBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer pk;
	
	@CsvBindByPosition(position = 0)
	@CsvBindByName(column = "Gezahlt")
	private String paid;

	@CsvBindByPosition(position = 1)
	@CsvBindByName(column = "Vorname")
	private String forename;

	@CsvBindByPosition(position = 2)
	@CsvBindByName(column = "Nachname")
	private String surname;

	@CsvBindByPosition(position = 3)
	@CsvBindByName(column = "Straße")
	private String street;

	@CsvBindByPosition(position = 4)
	@CsvBindByName(column = "PLZ")
	private String zip;

	@CsvBindByPosition(position = 5)
	@CsvBindByName(column = "Ort")
	private String city;

	@CsvBindByPosition(position = 6)
	@CsvBindByName(column = "Telefon")
	private String phone;

	@CsvBindByPosition(position = 7)
	@CsvBindByName(column = "E-Mail")
	private String email;

	@CsvBindByPosition(position = 8)
	@CsvBindByName(column = "Geburtstag")
	@CsvDate("dd.MM.yyyy")
	private Date birthdate;

	@CsvBindByPosition(position = 9)
	@CsvBindByName(column = "Geschlecht")
	private String sex;

	@CsvBindByPosition(position = 10)
	@CsvBindByName(column = "Rolle")
	private String camprole;

	@CsvBindByPosition(position = 11)
	@CsvBindByName(column = "Angenommen")
	private String accept;

	@CsvBindByPosition(position = 12)
	@CsvBindByName(column = "Kommentar")
	private String comment;

	@CsvBindByPosition(position = 13)
	@CsvBindByName(column = "Fotoeinverständnis")
	private Boolean consentCatalogPhoto;
	
	private Integer fkCamp;
	private Integer fkProfile;

	private List<CampBean> camps;

	/**
	 * generate csv headline
	 * 
	 * @return headline
	 */
	public static final String getHeadline() {
		return "Gezahlt,Vorname,Nachname,Straße,PLZ,Ort,Telefon,E-Mail,Geburtstag,Geschlecht,Rolle,Angenommen,Kommentar,Fotoeinverständnis\n";
	}

	public String getAddress() {
		return new StringBuilder("").append(street).append(", ").append(zip).append(" ").append(city).toString();
	}

	public String getFullname() {
		return new StringBuilder("").append(forename).append(" ").append(surname).toString();
	}

	public String getForename() {
		return forename;
	}

	public void setForename(String forename) {
		this.forename = forename;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Date getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(Date birthdate) {
		this.birthdate = birthdate;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCamprole() {
		return camprole;
	}

	public void setCamprole(String camprole) {
		this.camprole = camprole;
	}

	public Integer getFkCamp() {
		return fkCamp;
	}

	public void setFkCamp(Integer fkCamp) {
		this.fkCamp = fkCamp;
	}

	public List<CampBean> getCamps() {
		return camps;
	}

	public void setCamps(List<CampBean> camps) {
		this.camps = camps;
	}

	public Integer getFkProfile() {
		return fkProfile;
	}

	public void setFkProfile(Integer fkProfile) {
		this.fkProfile = fkProfile;
	}

	public Integer getPk() {
		return pk;
	}

	public void setPk(Integer pk) {
		this.pk = pk;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public EnumSex getSexEnum() {
		return new EnumConverter().getEnumSex(sex);
	}

	public EnumCamprole getCamproleEnum() {
		return new EnumConverter().getEnumCamprole(camprole);
	}

	public String getAccept() {
		return accept;
	}

	public void setAccept(Boolean accept) {
		this.accept = accept == null ? "unbearbeitet" : (accept ? "Ja" : "Nein");
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	/**
	 * @return the consentCatalogPhoto
	 */
	public Boolean getConsentCatalogPhoto() {
		return consentCatalogPhoto;
	}

	/**
	 * @param consentCatalogPhoto the consentCatalogPhoto to set
	 */
	public void setConsentCatalogPhoto(Boolean consentCatalogPhoto) {
		this.consentCatalogPhoto = consentCatalogPhoto;
	}
	
	public void setPaid(String paid) {
		this.paid = paid;
	}
	
	public String getPaid() {
		return paid;
	}
}
