package de.jottyfan.camporganizer.modules.admin;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.exception.DataAccessException;

import de.jottyfan.camporganizer.Controller;
import de.jottyfan.camporganizer.db.ProfileGateway;
import de.jottyfan.camporganizer.db.RssGateway;
import de.jottyfan.camporganizer.db.jooq.enums.EnumRole;
import de.jottyfan.camporganizer.profile.ProfileBean;
import de.jottyfan.camporganizer.rss.RssBean;

/**
 * 
 * @author jotty
 *
 */
@Named
@RequestScoped
public class AdminController extends Controller {

	private static final Logger LOGGER = LogManager.getLogger(AdminController.class);

	@Inject
	@Named("adminModel")
	private AdminModel model;

	public String toAdministrate() {
		FacesContext facesContext = FacesContext.getCurrentInstance();

		model.setProfileRole(new ProfileRoleBean(EnumRole.values()));
		try {
			ProfileGateway gw = new ProfileGateway(facesContext);
			model.setProfileRoles(gw.getAllProfileRoles());
			model.setRoles(gw.getAllRoles());
			model.setUsers(gw.getAllUsers());

			if (model.getRss() == null) {
				model.setRss(new RssBean(null));
			}
			model.setRssList(new RssGateway(facesContext).getAllRss());
		} catch (DataAccessException e) {
			facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "database error", e.getMessage()));
			LOGGER.error("AdminController.toAdministrate: ", e);
		}
		return "/pages/administrate.jsf";
	}

	public String doResetUserPassword(ProfileBean bean) {
		FacesContext facesContext = FacesContext.getCurrentInstance();

		model.setActiveIndex(1);
		try {
			String newPassword = new ProfileGateway(facesContext).resetPassword(bean);
			facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "password reset",
					"Das neue Passwort von " + bean.getFullname() + " lautet: " + newPassword));
		} catch (DataAccessException e) {
			facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "database error", e.getMessage()));
			LOGGER.error("AdminController.doResetUserPassword: ", e);
		}
		return toAdministrate();
	}

	public String doDeleteUser(ProfileBean bean) {
		FacesContext facesContext = FacesContext.getCurrentInstance();

		model.setActiveIndex(1);
		try {
			new ProfileGateway(facesContext).deleteProfile(bean);
		} catch (DataAccessException e) {
			facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "database error", e.getMessage()));
			LOGGER.error("AdminController.doDeleteUser: ", e);
		}
		return toAdministrate();
	}

	public String doDelete(ProfileRoleBean bean, String rolename) {
		FacesContext facesContext = FacesContext.getCurrentInstance();

		model.setActiveIndex(0);
		try {
			new ProfileGateway(facesContext).deleteProfileRole(bean.getUser().getPk(), rolename);
		} catch (DataAccessException e) {
			facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "database error", e.getMessage()));
			LOGGER.error("AdminController.doDelete: ", e);
		}
		return toAdministrate();
	}

	public String doResetDuedate(ProfileBean bean) {
		FacesContext facesContext = FacesContext.getCurrentInstance();

		model.setActiveIndex(1);
		try {
			new ProfileGateway(facesContext).updateDuedate(bean);
		} catch (DataAccessException e) {
			facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "database error", e.getMessage()));
			LOGGER.error("AdminController.doResetDuedate: ", e);
		}
		return toAdministrate();
	}

	public String doInsert(String rolename) {
		return doInsert(model.getProfileRole(), rolename);
	}
	
	public String doInsert(ProfileRoleBean bean, String rolename) {
		FacesContext facesContext = FacesContext.getCurrentInstance();

		model.setActiveIndex(0);
		try {
			new ProfileGateway(facesContext).insertProfileRole(bean.getUser().getPk(), rolename);
		} catch (DataAccessException e) {
			facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "database error", e.getMessage()));
			LOGGER.error("AdminController.doInsert: ", e);
		}
		return toAdministrate();
	}

	public String toEditRss(Integer pk) {
		model.setActiveIndex(5);
		for (RssBean bean : model.getRssList()) {
			if (bean.getPk().equals(pk)) {
				model.setRss(bean);
				model.setActiveIndexRss(1); // element tab
			}
		}
		return toAdministrate();
	}

	public String doDeleteRss() {
		FacesContext facesContext = FacesContext.getCurrentInstance();

		model.setActiveIndex(5);
		try {
			new RssGateway(facesContext).deleteRss(model.getRss());
			model.setActiveIndexRss(0);
			model.setRss(new RssBean(null));
		} catch (DataAccessException e) {
			facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "database error", e.getMessage()));
			LOGGER.error("AdminController.doDeleteRss: ", e);
		}
		return toAdministrate();
	}

	public String doUpdateRss() {
		FacesContext facesContext = FacesContext.getCurrentInstance();

		model.setActiveIndex(5);
		try {
			if (model.getRss().getPk() == null) {
				throw new DataAccessException("Anlegen von RSS-Feeds wird mit diesem Formular nicht unterstützt.");
			}
			new RssGateway(facesContext).update(model.getRss());
			model.setActiveIndexRss(0);
			model.setRss(new RssBean(null));
		} catch (DataAccessException e) {
			facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "database error", e.getMessage()));
			LOGGER.error("AdminController.doUpsertRss: ", e);
		}
		return toAdministrate();
	}

	public String doResetRss() {
		model.setActiveIndex(5);
		model.setActiveIndexRss(1);
		model.setRss(new RssBean(null));
		return toAdministrate();
	}

	public List<String> getPossibleRoles() {
		List<String> list = new ArrayList<>();
		for (EnumRole r : EnumRole.values()) {
			list.add(r.getLiteral());
		}
		list.sort((o1, o2) -> o1 == null ? 0 : o1.compareTo(o2));
		return list;
	}

	public void setModel(AdminModel model) {
		this.model = model;
	}
}
