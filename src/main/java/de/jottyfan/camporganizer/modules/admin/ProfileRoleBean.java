package de.jottyfan.camporganizer.modules.admin;

import java.util.ArrayList;
import java.util.List;

import de.jottyfan.camporganizer.db.jooq.enums.EnumRole;
import de.jottyfan.camporganizer.profile.ProfileBean;

/**
 * 
 * @author jotty
 *
 */
public class ProfileRoleBean {
	private ProfileBean user;
	private final List<String> roles;
	private final EnumRole[] availableRoles;

	public ProfileRoleBean(EnumRole[] availableRoles) {
		this.availableRoles = availableRoles;
		this.roles = new ArrayList<>();
	}

	public List<String> getNotRoles() {
		List<String> notRoles = new ArrayList<>();
		for (EnumRole role : availableRoles) {
			if (!roles.contains(role.getLiteral())) {
				notRoles.add(role.getLiteral());
			}
		}
		notRoles.sort((o1, o2) -> o1 == null ? 0 : o1.compareTo(o2));
		return notRoles;
	}

	public ProfileBean getUser() {
		return user;
	}

	public void setUser(ProfileBean user) {
		this.user = user;
	}

	public List<String> getRoles() {
		roles.sort((o1, o2) -> o1 == null ? 0 : o1.compareTo(o2));
		return roles;
	}

	public void addRole(String role) {
		this.roles.add(role);
	}
}
