package de.jottyfan.camporganizer.modules.businessman;

import java.math.BigDecimal;

/**
 * F
 * 
 * @author jotty
 *
 */
public class BudgetBean {
	private final String camp;
	private final Integer year;
	private final Float budget;

	public BudgetBean(String camp, Double year, BigDecimal budget) {
		super();
		this.camp = camp;
		this.year = year == null ? 0 : year.intValue();
		this.budget = budget == null ? 0.0f : budget.floatValue();
	}

	public String getCamp() {
		return camp;
	}

	public Float getBudget() {
		return budget;
	}
	
	public Integer getYear() {
		return year;
	}
}
