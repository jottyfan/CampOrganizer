package de.jottyfan.camporganizer.modules.businessman;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

/**
 * 
 * @author jotty
 *
 */
@Named
@SessionScoped
public class SalesModel implements Serializable {
	private static final long serialVersionUID = 1L;

	public SalesModel() {
		list = new ArrayList<>();
	}

	private SalesBean bean;
	private List<SalesBean> list;
	private List<BudgetBean> budget;
	private List<BudgetBean> yearly;

	private Integer activeIndex;

	public void clearBean() {
		this.bean = new SalesBean();
	}

	public List<SalesBean> getList() {
		return list;
	}

	public void setList(List<SalesBean> list) {
		this.list = list;
	}

	public Integer getActiveIndex() {
		return activeIndex;
	}

	public void setActiveIndex(Integer activeIndex) {
		this.activeIndex = activeIndex;
	}

	public List<BudgetBean> getBudget() {
		return budget;
	}

	public void setBudget(List<BudgetBean> budget) {
		this.budget = budget;
	}

	public SalesBean getBean() {
		return bean;
	}

	public void setBean(SalesBean bean) {
		this.bean = bean;
	}

	public List<BudgetBean> getYearly() {
		return yearly;
	}

	public void setYearly(List<BudgetBean> yearly) {
		this.yearly = yearly;
	}
}
