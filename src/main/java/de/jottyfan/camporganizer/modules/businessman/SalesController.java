package de.jottyfan.camporganizer.modules.businessman;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.exception.DataAccessException;

import de.jottyfan.camporganizer.Controller;
import de.jottyfan.camporganizer.db.CampGateway;
import de.jottyfan.camporganizer.db.SalesGateway;
import de.jottyfan.camporganizer.db.jooq.enums.EnumModule;
import de.jottyfan.camporganizer.profile.ProfileBean;

/**
 * 
 * @author jotty
 *
 */
@Named
@RequestScoped
public class SalesController extends Controller {
	private final static Logger LOGGER = LogManager.getLogger(SalesController.class);

	@Inject
	@Named("salesModel")
	private SalesModel model;
	
	@Inject
	private ProfileBean profileBean;

	private String toSales(Integer activeIndex) {
		FacesContext facesContext = FacesContext.getCurrentInstance();

		try {
			SalesGateway gw = new SalesGateway(facesContext);
			model.setBean(model.getBean() == null ? new SalesBean() : model.getBean());
			model.getBean().setCamps(new CampGateway(facesContext).getAllCampsFromView(false, profileBean.getPk(), EnumModule.business));
			model.getBean().setTraders(gw.getAllTraders());
			model.getBean().setProviders(gw.getAllProviders());
			model.setList(gw.getAllSales(profileBean.getPk()));
			model.setBudget(gw.getBudget());
			model.setYearly(gw.getYearly());
		} catch (DataAccessException e) {
			model.getBean().setCamps(new ArrayList<>());
			model.getBean().setTraders(new ArrayList<>());
			model.getBean().setProviders(new ArrayList<>());
			facesContext.addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "error on loading camps", e.getMessage()));
			LOGGER.error(e.getMessage(), e);
		}
		model.setActiveIndex(activeIndex);
		return "/pages/sales.jsf";
	}

	public String toSales() {
		return toSales(0);
	}

	public String toEdit(SalesBean selection) {
		model.setBean(selection);
		FacesContext facesContext = FacesContext.getCurrentInstance();
		try {
			model.getBean().setRecipeshot(new SalesGateway(facesContext).getRecipeShot(model.getBean().getPk()));
		} catch (DataAccessException e) {
			facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "error on loading recipe shot, saving may corrupt existing recipes", e.getMessage()));
		}
		return toSales();
	}
	
	public String doDelete(SalesBean selection) {
		FacesContext facesContext = FacesContext.getCurrentInstance();

		try {
			SalesGateway gw = new SalesGateway(facesContext);
			gw.delete(selection);
			model.setList(gw.getAllSales(profileBean.getPk()));
			return toSales(1);
		} catch (DataAccessException e) {
			facesContext.addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "error on deleting entry", e.getMessage()));
			return toSales(0);
		}
	}

	public String doUpsert() {
		FacesContext facesContext = FacesContext.getCurrentInstance();

		try {
			SalesGateway gw = new SalesGateway(facesContext);
			model.getBean().uploadFile();
			gw.upsert(model.getBean());
			model.setList(gw.getAllSales(profileBean.getPk()));
			model.clearBean();
			return toSales(1);
		} catch (DataAccessException | IOException e) {
			facesContext.addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "error on adding entry", e.getMessage()));
			return toSales(0);
		}
	}

	public String doDownload(SalesBean selection) {
		FacesContext facesContext = FacesContext.getCurrentInstance();

		try {
			byte[] data = new SalesGateway(facesContext).getRecipeShot(selection.getPk());
			
			if (data != null) {
				ExternalContext ec = facesContext.getExternalContext();
				ec.responseReset();
				ec.setResponseContentType(selection.getRecipeContentType());
				ec.setResponseContentLength(data.length);
				ec.setResponseHeader("Content-Disposition", "attachment; filename=\"" + selection.getRecipefilename() + "\"");
				OutputStream out = ec.getResponseOutputStream();
				out.write(data);
				out.flush();
				facesContext.responseComplete();
			} else {
				LOGGER.warn("image contains not data (is null)");
				facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "error on downloading file",
						"Kassenzettel enthält keinen Inhalt"));
			}
		} catch (IOException e) {
			LOGGER.error(e.getMessage(), e);
			facesContext.addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "error on downloading file", e.getMessage()));
		}
		return toSales(1);
	}

	public SalesBean getBean() {
		return model.getBean();
	}

	public void setModel(SalesModel model) {
		this.model = model;
	}

	public ProfileBean getProfileBean() {
		return profileBean;
	}

	public void setProfileBean(ProfileBean profileBean) {
		this.profileBean = profileBean;
	}
}
