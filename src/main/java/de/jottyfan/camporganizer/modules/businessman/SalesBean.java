package de.jottyfan.camporganizer.modules.businessman;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import javax.servlet.http.Part;

import org.apache.commons.io.IOUtils;

import de.jottyfan.camporganizer.CampBean;

/**
 * 
 * @author jotty
 *
 */
@Named
@SessionScoped
public class SalesBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer pk;
	private String trader;
	private Integer fkCamp;
	private String provider;
	private Float cash;
	private String incredients;
	private Date buydate;
	private String recipeNumber;
	private byte[] recipeshot;
	private Integer recipeshotSize;
	private String recipefilename;
	private String recipeNote;
	private Boolean lockSales;

	private Part file;

	private List<CampBean> camps;
	private List<String> traders;
	private List<String> providers;

	/**
	 * get name and year of camp
	 * 
	 * @param campId
	 *          to be used as a reference
	 * @return name and year if found, empty string otherwise
	 */
	public String getCampNameAndYear(Integer campId) {
		for (CampBean bean : camps) {
			if (bean.getPk().equals(campId)) {
				StringBuilder buf = new StringBuilder();
				buf.append(bean.getName()).append(" ").append(bean.getYear());
				return buf.toString();
			}
		}
		return "";
	}

	/**
	 * convert upload file content to recipeshot
	 * 
	 * @throws IOException
	 */
	public void uploadFile() throws IOException {
		if (file != null) {
			InputStream inputStream = file.getInputStream();
			byte[] bytes = IOUtils.toByteArray(inputStream);
			if (bytes.length > 0) {
				recipeshot = bytes;
				recipefilename = getFileName(file);
			} else {
				recipeshot = null;
				recipefilename = null;
			}
		}
	}

	/**
	 * get filename from part (from
	 * https://stackoverflow.com/questions/30810014/javax-servlet-http-part-to-java-io-file)
	 * 
	 * @param part
	 *          the part
	 * @return the filename if found or null
	 */
	private String getFileName(final Part part) {
		final String partHeader = part.getHeader("content-disposition");
		for (String content : partHeader.split(";")) {
			if (content.trim().startsWith("filename")) {
				return content.substring(content.indexOf('=') + 1).trim().replace("\"", "");
			}
		}
		return null;
	}

	/**
	 * get all unlocked camp beans
	 * 
	 * @return list of unlocked camp beans
	 */
	public List<CampBean> getUnlockedCamps() {
		List<CampBean> list = new ArrayList<>();
		for (CampBean bean : camps) {
			if (!bean.getLockSales()) {
				list.add(bean);
			}
		}
		return list;
	}

	/**
	 * get mime type from file name ending
	 * 
	 * @return the found mime type, the default if not supported or null on empty
	 *         filename
	 */
	public String getRecipeContentType() {
		if (recipefilename == null) {
			return null;
		} else if (recipefilename.toLowerCase().trim().endsWith(".pdf")) {
			return "application/pdf";
		} else if (recipefilename.toLowerCase().trim().endsWith(".jpg")
				|| recipefilename.toLowerCase().trim().endsWith(".jpeg")) {
			return "image/jpeg";
		} else if (recipefilename.toLowerCase().trim().endsWith(".png")) {
			return "image/png";
		} else {
			return "application/octet-stream";
		}
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SalesBean [trader=");
		builder.append(trader);
		builder.append(", fkCamp=");
		builder.append(fkCamp);
		builder.append(", provider=");
		builder.append(provider);
		builder.append(", cash=");
		builder.append(cash);
		builder.append(", incredients=");
		builder.append(incredients);
		builder.append(", buydate=");
		builder.append(buydate);
		builder.append(", recipeNumber=");
		builder.append(recipeNumber);
		builder.append(", recipefilename=");
		builder.append(recipefilename);
		builder.append(", recipeNote=");
		builder.append(recipeNote);
		builder.append(", lockSales=");
		builder.append(lockSales);
		builder.append("]");
		return builder.toString();
	}

	public String getTrader() {
		return trader;
	}

	public void setTrader(String trader) {
		this.trader = trader;
	}

	public Integer getFkCamp() {
		return fkCamp;
	}

	public void setFkCamp(Integer fkCamp) {
		this.fkCamp = fkCamp;
	}

	public String getProvider() {
		return provider;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}

	public Float getCash() {
		return cash;
	}

	public BigDecimal getCashBigDecimal() {
		return cash == null ? null : new BigDecimal(cash);
	}

	public void setCash(Float cash) {
		this.cash = cash;
	}

	public Date getBuydate() {
		return buydate;
	}

	public void setBuydate(Date buydate) {
		this.buydate = buydate;
	}

	public String getRecipeNumber() {
		return recipeNumber;
	}

	public void setRecipeNumber(String recipeNumber) {
		this.recipeNumber = recipeNumber;
	}

	public String getRecipeNote() {
		return recipeNote;
	}

	public void setRecipeNote(String recipeNote) {
		this.recipeNote = recipeNote;
	}

	public List<CampBean> getCamps() {
		return camps;
	}

	public void setCamps(List<CampBean> camps) {
		this.camps = camps;
	}

	public String getIncredients() {
		return incredients;
	}

	public void setIncredients(String incredients) {
		this.incredients = incredients;
	}

	public byte[] getRecipeshot() {
		return recipeshot;
	}

	public void setRecipeshot(byte[] recipeshot) {
		this.recipeshot = recipeshot;
	}

	public Part getFile() {
		return file;
	}

	public void setFile(Part file) {
		this.file = file;
	}

	public Integer getPk() {
		return pk;
	}

	public void setPk(Integer pk) {
		this.pk = pk;
	}

	public List<String> getTraders() {
		return traders;
	}

	public void setTraders(List<String> traders) {
		this.traders = traders;
	}

	public List<String> getProviders() {
		return providers;
	}

	public void setProviders(List<String> providers) {
		this.providers = providers;
	}

	public Boolean getLockSales() {
		return lockSales;
	}

	public void setLockSales(Boolean lockSales) {
		this.lockSales = lockSales;
	}

	public String getRecipefilename() {
		return recipefilename;
	}

	public void setRecipefilename(String recipefilename) {
		this.recipefilename = recipefilename;
	}

	/**
	 * @return the recipeshotSize
	 */
	public Integer getRecipeshotSize() {
		return recipeshotSize;
	}

	/**
	 * @param recipeshotSize the recipeshotSize to set
	 */
	public void setRecipeshotSize(Integer recipeshotSize) {
		this.recipeshotSize = recipeshotSize;
	}
}
