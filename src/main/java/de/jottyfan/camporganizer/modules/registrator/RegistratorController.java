package de.jottyfan.camporganizer.modules.registrator;

import java.io.IOException;

import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import de.jottyfan.camporganizer.Controller;
import de.jottyfan.camporganizer.db.PersondocumentGateway;
import de.jottyfan.camporganizer.modules.subscriber.PersondocumentBean;
import de.jottyfan.camporganizer.profile.ProfileBean;

/**
 *
 * @author jotty
 *
 */
@Named
@RequestScoped
public class RegistratorController extends Controller {

	@Inject
	@Named("registratorModel")
	private RegistratorModel model;

	@Inject
	private ProfileBean profileBean;

	public String toMain() {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		model.loadRegistratorPageContent(facesContext, profileBean);
		return "/pages/registrator/main.jsf";
	}

	public String toEdit(RegistratorBean bean) {
		model.prepareEdit(bean);
		return "/pages/registrator/edit.jsf";
	}

	public String doUpdate() {
		FacesContext facesContext = FacesContext.getCurrentInstance();

		model.update(facesContext, profileBean);
		return toMain();
	}

	public String doAccept(RegistratorBean bean) {
		FacesContext facesContext = FacesContext.getCurrentInstance();

		model.acceptRegistration(facesContext, bean, profileBean);
		return toMain();
	}

	public String doDelete(Integer pk) {
		FacesContext facesContext = FacesContext.getCurrentInstance();

		model.deleteRegistration(facesContext, pk);
		return toMain();
	}

	public String doReject(RegistratorBean bean) {
		FacesContext facesContext = FacesContext.getCurrentInstance();

		model.rejectRegistration(facesContext, bean, profileBean);
		return toMain();
	}

	public String doReset(RegistratorBean bean) {
		FacesContext facesContext = FacesContext.getCurrentInstance();

		model.resetRegistration(facesContext, bean, profileBean);
		return toMain();
	}

	public String doDownloadDocument(PersondocumentBean d) {
		FacesContext facesContext = FacesContext.getCurrentInstance();

		super.doDownloadBase64(facesContext, d.getDocument(), d.getName(), d.getFiletype().getLiteral());
		return toMain();
	}

	public String doDeleteDocument(PersondocumentBean bean) {
		FacesContext facesContext = FacesContext.getCurrentInstance();

		Integer result = new PersondocumentGateway(facesContext).deletePersondocument(bean);
		return toRegister();
	}

	public String doDownloadCampAsCsv() {
		FacesContext facesContext = FacesContext.getCurrentInstance();

		model.doDownloadCsv(facesContext);
		return toMain();
	}

	public RegistratorModel getModel() {
		return model;
	}

	public void setModel(RegistratorModel model) {
		this.model = model;
	}

	public void setProfileBean(ProfileBean profileBean) {
		this.profileBean = profileBean;
	}
}
