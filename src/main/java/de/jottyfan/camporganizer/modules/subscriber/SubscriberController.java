package de.jottyfan.camporganizer.modules.subscriber;

import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import de.jottyfan.camporganizer.Controller;
import de.jottyfan.camporganizer.modules.campadmin.DocumentBean;
import de.jottyfan.camporganizer.profile.ProfileBean;

/**
 * 
 * @author jotty
 *
 */
@Named
@RequestScoped
public class SubscriberController extends Controller {

	@Inject
	@Named("subscriberModel")
	private SubscriberModel model;

	@Inject
	private ProfileBean profileBean;

	public String toMain() {
		FacesContext facesContext = FacesContext.getCurrentInstance();

		model.load(facesContext, profileBean.getPk());
		return "/pages/subscriber/main.jsf";
	}

	public String doDelete(SubscriberBean bean) {
		FacesContext facesContext = FacesContext.getCurrentInstance();

		model.doDelete(facesContext, profileBean, bean);
		return toMain();
	}
	
	public String doSetConsent(SubscriberBean bean) {
		FacesContext facesContext = FacesContext.getCurrentInstance();

		bean = model.doSetConsent(facesContext, bean);
		return toEdit(bean);
	}

	public String doDownloadDocument(PersondocumentBean d) {
		FacesContext facesContext = FacesContext.getCurrentInstance();

		String filetype = d.getFiletype() != null ? d.getFiletype().getLiteral() : "other";
		super.doDownloadBase64(facesContext, d.getDocument(), d.getName(), filetype);
		return toMain();
	}

	public String doDownloadDocument(DocumentBean d) {
		FacesContext facesContext = FacesContext.getCurrentInstance();

		String filetype = d.getFiletype() != null ? d.getFiletype().getLiteral() : "other";
		super.doDownloadBase64(facesContext, d.getDocument(), d.getName(), filetype);
		return toMain();
	}

	public String doDeleteUserDoc(PersondocumentBean d) {
		FacesContext facesContext = FacesContext.getCurrentInstance();

		model.doDeleteUserDoc(facesContext, d);
		return toMain();
	}

	public String doAddUserDoc() {
		FacesContext facesContext = FacesContext.getCurrentInstance();

		model.doAddUserDoc(facesContext);
		return toMain();
	}

	public String toEdit(SubscriberBean bean) {
		FacesContext facesContext = FacesContext.getCurrentInstance();

		model.loadPerson(facesContext, bean.getPkPerson());
		return "/pages/subscriber/edit.jsf";
	}

	public String doUpdate() {
		FacesContext facesContext = FacesContext.getCurrentInstance();

		model.doUpdate(facesContext);
		return toMain();
	}

	public void setModel(SubscriberModel model) {
		this.model = model;
	}

	public void setProfileBean(ProfileBean profileBean) {
		this.profileBean = profileBean;
	}
}
