package de.jottyfan.camporganizer;

/**
 * 
 * @author jotty
 *
 */
public class LambdaResultWrapper {
	private Integer number;

	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	public void addToNumber(Integer number) {
		this.number = this.number == null ? number : this.number + number;
	}
}
