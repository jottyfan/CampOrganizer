package de.jottyfan.camporganizer.db;

import static de.jottyfan.camporganizer.db.jooq.Tables.T_CAMP;
import static de.jottyfan.camporganizer.db.jooq.Tables.T_CAMPDOCUMENT;
import static de.jottyfan.camporganizer.db.jooq.Tables.T_DOCUMENT;
import static de.jottyfan.camporganizer.db.jooq.Tables.T_DOCUMENTROLE;
import static de.jottyfan.camporganizer.db.jooq.Tables.T_LOCATION;
import static de.jottyfan.camporganizer.db.jooq.Tables.T_PERSONDOCUMENT;
import static de.jottyfan.camporganizer.db.jooq.Tables.T_RSS;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.faces.context.FacesContext;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.DeleteConditionStep;
import org.jooq.InsertResultStep;
import org.jooq.InsertReturningStep;
import org.jooq.InsertValuesStep2;
import org.jooq.Record5;
import org.jooq.SelectConditionStep;
import org.jooq.UpdateConditionStep;
import org.jooq.exception.DataAccessException;
import org.jooq.impl.DSL;

import de.jottyfan.camporganizer.LambdaResultWrapper;
import de.jottyfan.camporganizer.db.jooq.enums.EnumCamprole;
import de.jottyfan.camporganizer.db.jooq.enums.EnumDocument;
import de.jottyfan.camporganizer.db.jooq.enums.EnumFiletype;
import de.jottyfan.camporganizer.db.jooq.tables.records.TCampRecord;
import de.jottyfan.camporganizer.db.jooq.tables.records.TCampdocumentRecord;
import de.jottyfan.camporganizer.db.jooq.tables.records.TDocumentRecord;
import de.jottyfan.camporganizer.db.jooq.tables.records.TDocumentroleRecord;
import de.jottyfan.camporganizer.db.jooq.tables.records.TLocationRecord;
import de.jottyfan.camporganizer.db.jooq.tables.records.TPersondocumentRecord;
import de.jottyfan.camporganizer.db.jooq.tables.records.TRssRecord;
import de.jottyfan.camporganizer.modules.campadmin.DocumentBean;

/**
 * 
 * @author henkej
 *
 */
public class DocumentGateway extends JooqGateway {

	private static final Logger LOGGER = LogManager.getLogger(DocumentGateway.class);

	public DocumentGateway(FacesContext facesContext) throws DataAccessException {
		super(facesContext);
	}

	/**
	 * delete entry from t_document where pk = ?
	 * 
	 * @param pk
	 *          to be used as reference
	 * @return number of affected database lines
	 * @throws DataAccessException
	 */
	public Integer deleteDocument(Integer pk) throws DataAccessException {
		LambdaResultWrapper lrw = new LambdaResultWrapper();
		getJooq().transaction(t -> {
			UpdateConditionStep<TCampRecord> sql = DSL.using(t)
			// @formatter:off
			  .update(T_CAMP)
			  .set(T_CAMP.FK_DOCUMENT, (Integer) null)
			  .where(T_CAMP.FK_DOCUMENT.eq(pk));
		  // @formatter:on
			LOGGER.debug("{}", sql.toString());
			lrw.addToNumber(sql.execute());

			UpdateConditionStep<TLocationRecord> sql1 = DSL.using(t)
			// @formatter:off
			  .update(T_LOCATION)
			  .set(T_LOCATION.FK_DOCUMENT, (Integer) null)
			  .where(T_LOCATION.FK_DOCUMENT.eq(pk));
		  // @formatter:on
			LOGGER.debug("{}", sql1.toString());
			lrw.addToNumber(sql1.execute());

			DeleteConditionStep<TDocumentroleRecord> sql2 = DSL.using(t)
			// @formatter:off
				.deleteFrom(T_DOCUMENTROLE)
				.where(T_DOCUMENTROLE.FK_DOCUMENT.eq(pk));
			// @formatter:on
			LOGGER.debug("{}", sql2.toString());
			lrw.addToNumber(sql2.execute());

			DeleteConditionStep<TDocumentRecord> sql3 = DSL.using(t)
			// @formatter:off
				.deleteFrom(T_DOCUMENT)
				.where(T_DOCUMENT.PK.eq(pk));
			// @formatter:on
			LOGGER.debug("{}", sql3.toString());
			lrw.addToNumber(sql3.execute());
		});
		return lrw.getNumber();
	}

	/**
	 * upsert document in t_document
	 * 
	 * @param document
	 * @throws DataAccessException
	 */
	public Integer upsert(DocumentBean bean) throws DataAccessException {
		LambdaResultWrapper lrw = new LambdaResultWrapper();
		getJooq().transaction(c -> {
			Integer pk = bean.getPk();
			if (bean.getPk() != null) {
				UpdateConditionStep<TDocumentRecord> sql = DSL.using(c)
				// @formatter:off
					.update(T_DOCUMENT)
					.set(T_DOCUMENT.NAME, bean.getName())
					.set(T_DOCUMENT.DOCTYPE, bean.getDoctype())
					.set(T_DOCUMENT.DOCUMENT, bean.getDocument())
					.set(T_DOCUMENT.FILETYPE, bean.getFiletype())
					.where(T_DOCUMENT.PK.eq(bean.getPk()));
			  // @formatter:on
				LOGGER.debug("{}", sql.toString());
				lrw.addToNumber(sql.execute());
			} else {
				InsertResultStep<TDocumentRecord> sql = DSL.using(c)
				// @formatter:off
				  .insertInto(T_DOCUMENT,
				  		        T_DOCUMENT.NAME,
				  		        T_DOCUMENT.DOCTYPE,
				  		        T_DOCUMENT.DOCUMENT,
				  		        T_DOCUMENT.FILETYPE)
				  .values(bean.getName(), bean.getDoctype(), bean.getDocument(), bean.getFiletype())
				  .returning(T_DOCUMENT.PK);
		  	// @formatter:on
				LOGGER.debug("{}", sql.toString());
				pk = sql.fetchOne().get(T_DOCUMENT.PK);
				lrw.addToNumber(1);
			}
			String[] roles = bean.getRoles().split(",");
			List<EnumCamprole> allEnums = Arrays.asList(EnumCamprole.values());
			Set<EnumCamprole> removeCandidates = new HashSet<>();
			removeCandidates.addAll(allEnums);
			for (String role : roles) {
				try {
					EnumCamprole camprole = EnumCamprole.valueOf(role);
					InsertReturningStep<TDocumentroleRecord> sql = DSL.using(c)
				  // @formatter:off
				  	.insertInto(T_DOCUMENTROLE,
				  			        T_DOCUMENTROLE.FK_DOCUMENT,
				  			        T_DOCUMENTROLE.CAMPROLE)
				  	.values(pk, camprole)
				  	.onConflict(T_DOCUMENTROLE.FK_DOCUMENT, T_DOCUMENTROLE.CAMPROLE)
				  	.doNothing();
				  // @formatter:on
					LOGGER.debug("{}", sql.toString());
					lrw.addToNumber(sql.execute());
					removeCandidates.remove(camprole);
				} catch (IllegalArgumentException e) {
					LOGGER.error(e);
				}
			}
			DeleteConditionStep<TDocumentroleRecord> sql = DSL.using(c)
			// @formatter:off
				.deleteFrom(T_DOCUMENTROLE)
				.where(T_DOCUMENTROLE.FK_DOCUMENT.eq(pk))
				.and(T_DOCUMENTROLE.CAMPROLE.in(removeCandidates));
			// @formatter:on
			LOGGER.debug("{}", sql.toString());
			lrw.addToNumber(sql.execute());
		});
		return lrw.getNumber();
	}

	/**
	 * delete document from t_persondocument
	 * 
	 * @param pk
	 * @throws DataAccessException
	 */
	public void deletePersonDocument(Integer pk) throws DataAccessException {
		getJooq().transaction(t -> {
			DeleteConditionStep<TPersondocumentRecord> sql = DSL.using(t)
			// @formatter:off
				.deleteFrom(T_PERSONDOCUMENT)
				.where(T_PERSONDOCUMENT.PK.eq(pk));
			// @formatter:on
			LOGGER.debug("{}", sql.toString());
			sql.execute();

			StringBuilder buf = new StringBuilder("Dokument ");
			buf.append(pk);
			buf.append(" wurde wieder gelöscht.");
			InsertValuesStep2<TRssRecord, String, String> sql2 = DSL.using(t)
			// @formatter:off
		    .insertInto(T_RSS,
		    		         T_RSS.MSG,
		    		         T_RSS.RECIPIENT)
		    .values(buf.toString(), "registrator");
		  // @formatter:on
			LOGGER.debug("{}", sql2.toString());
			sql2.execute();
		});
	}

	/**
	 * load all camp documents for pk = camp
	 * 
	 * @param pk
	 *          id of the camp
	 * @return list of found documents; an empty list at least
	 */
	public List<DocumentBean> getCampDocuments(Integer pk) {
		SelectConditionStep<Record5<Integer, String, EnumDocument, EnumFiletype, String>> sql = getJooq()
		// @formatter:off
			.select(T_DOCUMENT.PK,
					    T_DOCUMENT.NAME,
					    T_DOCUMENT.DOCTYPE,
					    T_DOCUMENT.FILETYPE,
					    T_DOCUMENT.DOCUMENT)
			.from(T_CAMPDOCUMENT)
			.leftJoin(T_DOCUMENT).on(T_DOCUMENT.PK.eq(T_CAMPDOCUMENT.FK_DOCUMENT))
			.where(T_CAMPDOCUMENT.FK_CAMP.eq(pk));
		// @formatter:on
		LOGGER.debug("{}", sql.toString());
		List<DocumentBean> list = new ArrayList<>();
		for (Record5<Integer, String, EnumDocument, EnumFiletype, String> r : sql.fetch()) {
			Integer pkDoc = r.get(T_DOCUMENT.PK);
			DocumentBean bean = new DocumentBean(pkDoc);
			bean.setDoctype(r.get(T_DOCUMENT.DOCTYPE));
			bean.setDocument(r.get(T_DOCUMENT.DOCUMENT));
			bean.setFiletype(r.get(T_DOCUMENT.FILETYPE));
			bean.setName(r.get(T_DOCUMENT.NAME));
			// TODO: add documentroles
			list.add(bean);
		}
		return list;
	}

	/**
	 * remove document from camp
	 * 
	 * @param fkDocument
	 *          the document pk
	 * @param fkCamp
	 *          the camp pk
	 * @return number of affected database rows, should be 1s
	 */
	public Integer removeDocumentFromCamp(Integer fkDocument, Integer fkCamp) {
		DeleteConditionStep<TCampdocumentRecord> sql = getJooq()
		// @formatter:off
			.deleteFrom(T_CAMPDOCUMENT)
			.where(T_CAMPDOCUMENT.FK_CAMP.eq(fkCamp))
			.and(T_CAMPDOCUMENT.FK_DOCUMENT.eq(fkDocument));
		// @formatter:on
		LOGGER.debug("{}", sql.toString());
		return sql.execute();
	}

	/**
	 * add document to camp
	 * 
	 * @param fkDocument
	 *          the document pk
	 * @param fkCamp
	 *          the camp pk
	 * @return number of affected database rows, should be 1
	 */
	public Integer addDocumentToCamp(Integer fkDocument, Integer fkCamp) {
		InsertValuesStep2<TCampdocumentRecord, Integer, Integer> sql = getJooq()
		// @formatter:off
			.insertInto(T_CAMPDOCUMENT,
					        T_CAMPDOCUMENT.FK_CAMP,
					        T_CAMPDOCUMENT.FK_DOCUMENT)
			.values(fkCamp, fkDocument);
		// @formatter:on
		LOGGER.debug("{}", sql.toString());
		return sql.execute();
	}
}
