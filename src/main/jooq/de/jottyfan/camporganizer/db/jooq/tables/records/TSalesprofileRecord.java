/*
 * This file is generated by jOOQ.
 */
package de.jottyfan.camporganizer.db.jooq.tables.records;


import de.jottyfan.camporganizer.db.jooq.tables.TSalesprofile;

import javax.annotation.processing.Generated;

import org.jooq.Field;
import org.jooq.Record3;
import org.jooq.Row3;
import org.jooq.impl.TableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "https://www.jooq.org",
        "jOOQ version:3.13.5"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class TSalesprofileRecord extends TableRecordImpl<TSalesprofileRecord> implements Record3<Integer, Integer, Integer> {

    private static final long serialVersionUID = 932061648;

    /**
     * Setter for <code>camp.t_salesprofile.pk</code>.
     */
    public void setPk(Integer value) {
        set(0, value);
    }

    /**
     * Getter for <code>camp.t_salesprofile.pk</code>.
     */
    public Integer getPk() {
        return (Integer) get(0);
    }

    /**
     * Setter for <code>camp.t_salesprofile.fk_camp</code>.
     */
    public void setFkCamp(Integer value) {
        set(1, value);
    }

    /**
     * Getter for <code>camp.t_salesprofile.fk_camp</code>.
     */
    public Integer getFkCamp() {
        return (Integer) get(1);
    }

    /**
     * Setter for <code>camp.t_salesprofile.fk_profile</code>.
     */
    public void setFkProfile(Integer value) {
        set(2, value);
    }

    /**
     * Getter for <code>camp.t_salesprofile.fk_profile</code>.
     */
    public Integer getFkProfile() {
        return (Integer) get(2);
    }

    // -------------------------------------------------------------------------
    // Record3 type implementation
    // -------------------------------------------------------------------------

    @Override
    public Row3<Integer, Integer, Integer> fieldsRow() {
        return (Row3) super.fieldsRow();
    }

    @Override
    public Row3<Integer, Integer, Integer> valuesRow() {
        return (Row3) super.valuesRow();
    }

    @Override
    public Field<Integer> field1() {
        return TSalesprofile.T_SALESPROFILE.PK;
    }

    @Override
    public Field<Integer> field2() {
        return TSalesprofile.T_SALESPROFILE.FK_CAMP;
    }

    @Override
    public Field<Integer> field3() {
        return TSalesprofile.T_SALESPROFILE.FK_PROFILE;
    }

    @Override
    public Integer component1() {
        return getPk();
    }

    @Override
    public Integer component2() {
        return getFkCamp();
    }

    @Override
    public Integer component3() {
        return getFkProfile();
    }

    @Override
    public Integer value1() {
        return getPk();
    }

    @Override
    public Integer value2() {
        return getFkCamp();
    }

    @Override
    public Integer value3() {
        return getFkProfile();
    }

    @Override
    public TSalesprofileRecord value1(Integer value) {
        setPk(value);
        return this;
    }

    @Override
    public TSalesprofileRecord value2(Integer value) {
        setFkCamp(value);
        return this;
    }

    @Override
    public TSalesprofileRecord value3(Integer value) {
        setFkProfile(value);
        return this;
    }

    @Override
    public TSalesprofileRecord values(Integer value1, Integer value2, Integer value3) {
        value1(value1);
        value2(value2);
        value3(value3);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached TSalesprofileRecord
     */
    public TSalesprofileRecord() {
        super(TSalesprofile.T_SALESPROFILE);
    }

    /**
     * Create a detached, initialised TSalesprofileRecord
     */
    public TSalesprofileRecord(Integer pk, Integer fkCamp, Integer fkProfile) {
        super(TSalesprofile.T_SALESPROFILE);

        set(0, pk);
        set(1, fkCamp);
        set(2, fkProfile);
    }
}
