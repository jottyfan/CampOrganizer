/*
 * This file is generated by jOOQ.
 */
package de.jottyfan.camporganizer.db.jooq.tables.records;


import de.jottyfan.camporganizer.db.jooq.enums.EnumCamprole;
import de.jottyfan.camporganizer.db.jooq.tables.VDsgvoDeleteCandidate;

import javax.annotation.processing.Generated;

import org.jooq.Field;
import org.jooq.Record9;
import org.jooq.Row9;
import org.jooq.impl.TableRecordImpl;
import org.jooq.types.YearToSecond;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "https://www.jooq.org",
        "jOOQ version:3.13.5"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class VDsgvoDeleteCandidateRecord extends TableRecordImpl<VDsgvoDeleteCandidateRecord> implements Record9<Integer, String, String, Integer, YearToSecond, EnumCamprole, String, Double, Boolean> {

    private static final long serialVersionUID = 990362144;

    /**
     * Setter for <code>camp.v_dsgvo_delete_candidate.fk_person</code>.
     */
    public void setFkPerson(Integer value) {
        set(0, value);
    }

    /**
     * Getter for <code>camp.v_dsgvo_delete_candidate.fk_person</code>.
     */
    public Integer getFkPerson() {
        return (Integer) get(0);
    }

    /**
     * Setter for <code>camp.v_dsgvo_delete_candidate.forename</code>.
     */
    public void setForename(String value) {
        set(1, value);
    }

    /**
     * Getter for <code>camp.v_dsgvo_delete_candidate.forename</code>.
     */
    public String getForename() {
        return (String) get(1);
    }

    /**
     * Setter for <code>camp.v_dsgvo_delete_candidate.surname</code>.
     */
    public void setSurname(String value) {
        set(2, value);
    }

    /**
     * Getter for <code>camp.v_dsgvo_delete_candidate.surname</code>.
     */
    public String getSurname() {
        return (String) get(2);
    }

    /**
     * Setter for <code>camp.v_dsgvo_delete_candidate.fk_camp</code>.
     */
    public void setFkCamp(Integer value) {
        set(3, value);
    }

    /**
     * Getter for <code>camp.v_dsgvo_delete_candidate.fk_camp</code>.
     */
    public Integer getFkCamp() {
        return (Integer) get(3);
    }

    /**
     * Setter for <code>camp.v_dsgvo_delete_candidate.age</code>.
     */
    public void setAge(YearToSecond value) {
        set(4, value);
    }

    /**
     * Getter for <code>camp.v_dsgvo_delete_candidate.age</code>.
     */
    public YearToSecond getAge() {
        return (YearToSecond) get(4);
    }

    /**
     * Setter for <code>camp.v_dsgvo_delete_candidate.camprole</code>.
     */
    public void setCamprole(EnumCamprole value) {
        set(5, value);
    }

    /**
     * Getter for <code>camp.v_dsgvo_delete_candidate.camprole</code>.
     */
    public EnumCamprole getCamprole() {
        return (EnumCamprole) get(5);
    }

    /**
     * Setter for <code>camp.v_dsgvo_delete_candidate.name</code>.
     */
    public void setName(String value) {
        set(6, value);
    }

    /**
     * Getter for <code>camp.v_dsgvo_delete_candidate.name</code>.
     */
    public String getName() {
        return (String) get(6);
    }

    /**
     * Setter for <code>camp.v_dsgvo_delete_candidate.year</code>.
     */
    public void setYear(Double value) {
        set(7, value);
    }

    /**
     * Getter for <code>camp.v_dsgvo_delete_candidate.year</code>.
     */
    public Double getYear() {
        return (Double) get(7);
    }

    /**
     * Setter for <code>camp.v_dsgvo_delete_candidate.is_over</code>.
     */
    public void setIsOver(Boolean value) {
        set(8, value);
    }

    /**
     * Getter for <code>camp.v_dsgvo_delete_candidate.is_over</code>.
     */
    public Boolean getIsOver() {
        return (Boolean) get(8);
    }

    // -------------------------------------------------------------------------
    // Record9 type implementation
    // -------------------------------------------------------------------------

    @Override
    public Row9<Integer, String, String, Integer, YearToSecond, EnumCamprole, String, Double, Boolean> fieldsRow() {
        return (Row9) super.fieldsRow();
    }

    @Override
    public Row9<Integer, String, String, Integer, YearToSecond, EnumCamprole, String, Double, Boolean> valuesRow() {
        return (Row9) super.valuesRow();
    }

    @Override
    public Field<Integer> field1() {
        return VDsgvoDeleteCandidate.V_DSGVO_DELETE_CANDIDATE.FK_PERSON;
    }

    @Override
    public Field<String> field2() {
        return VDsgvoDeleteCandidate.V_DSGVO_DELETE_CANDIDATE.FORENAME;
    }

    @Override
    public Field<String> field3() {
        return VDsgvoDeleteCandidate.V_DSGVO_DELETE_CANDIDATE.SURNAME;
    }

    @Override
    public Field<Integer> field4() {
        return VDsgvoDeleteCandidate.V_DSGVO_DELETE_CANDIDATE.FK_CAMP;
    }

    @Override
    public Field<YearToSecond> field5() {
        return VDsgvoDeleteCandidate.V_DSGVO_DELETE_CANDIDATE.AGE;
    }

    @Override
    public Field<EnumCamprole> field6() {
        return VDsgvoDeleteCandidate.V_DSGVO_DELETE_CANDIDATE.CAMPROLE;
    }

    @Override
    public Field<String> field7() {
        return VDsgvoDeleteCandidate.V_DSGVO_DELETE_CANDIDATE.NAME;
    }

    @Override
    public Field<Double> field8() {
        return VDsgvoDeleteCandidate.V_DSGVO_DELETE_CANDIDATE.YEAR;
    }

    @Override
    public Field<Boolean> field9() {
        return VDsgvoDeleteCandidate.V_DSGVO_DELETE_CANDIDATE.IS_OVER;
    }

    @Override
    public Integer component1() {
        return getFkPerson();
    }

    @Override
    public String component2() {
        return getForename();
    }

    @Override
    public String component3() {
        return getSurname();
    }

    @Override
    public Integer component4() {
        return getFkCamp();
    }

    @Override
    public YearToSecond component5() {
        return getAge();
    }

    @Override
    public EnumCamprole component6() {
        return getCamprole();
    }

    @Override
    public String component7() {
        return getName();
    }

    @Override
    public Double component8() {
        return getYear();
    }

    @Override
    public Boolean component9() {
        return getIsOver();
    }

    @Override
    public Integer value1() {
        return getFkPerson();
    }

    @Override
    public String value2() {
        return getForename();
    }

    @Override
    public String value3() {
        return getSurname();
    }

    @Override
    public Integer value4() {
        return getFkCamp();
    }

    @Override
    public YearToSecond value5() {
        return getAge();
    }

    @Override
    public EnumCamprole value6() {
        return getCamprole();
    }

    @Override
    public String value7() {
        return getName();
    }

    @Override
    public Double value8() {
        return getYear();
    }

    @Override
    public Boolean value9() {
        return getIsOver();
    }

    @Override
    public VDsgvoDeleteCandidateRecord value1(Integer value) {
        setFkPerson(value);
        return this;
    }

    @Override
    public VDsgvoDeleteCandidateRecord value2(String value) {
        setForename(value);
        return this;
    }

    @Override
    public VDsgvoDeleteCandidateRecord value3(String value) {
        setSurname(value);
        return this;
    }

    @Override
    public VDsgvoDeleteCandidateRecord value4(Integer value) {
        setFkCamp(value);
        return this;
    }

    @Override
    public VDsgvoDeleteCandidateRecord value5(YearToSecond value) {
        setAge(value);
        return this;
    }

    @Override
    public VDsgvoDeleteCandidateRecord value6(EnumCamprole value) {
        setCamprole(value);
        return this;
    }

    @Override
    public VDsgvoDeleteCandidateRecord value7(String value) {
        setName(value);
        return this;
    }

    @Override
    public VDsgvoDeleteCandidateRecord value8(Double value) {
        setYear(value);
        return this;
    }

    @Override
    public VDsgvoDeleteCandidateRecord value9(Boolean value) {
        setIsOver(value);
        return this;
    }

    @Override
    public VDsgvoDeleteCandidateRecord values(Integer value1, String value2, String value3, Integer value4, YearToSecond value5, EnumCamprole value6, String value7, Double value8, Boolean value9) {
        value1(value1);
        value2(value2);
        value3(value3);
        value4(value4);
        value5(value5);
        value6(value6);
        value7(value7);
        value8(value8);
        value9(value9);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached VDsgvoDeleteCandidateRecord
     */
    public VDsgvoDeleteCandidateRecord() {
        super(VDsgvoDeleteCandidate.V_DSGVO_DELETE_CANDIDATE);
    }

    /**
     * Create a detached, initialised VDsgvoDeleteCandidateRecord
     */
    public VDsgvoDeleteCandidateRecord(Integer fkPerson, String forename, String surname, Integer fkCamp, YearToSecond age, EnumCamprole camprole, String name, Double year, Boolean isOver) {
        super(VDsgvoDeleteCandidate.V_DSGVO_DELETE_CANDIDATE);

        set(0, fkPerson);
        set(1, forename);
        set(2, surname);
        set(3, fkCamp);
        set(4, age);
        set(5, camprole);
        set(6, name);
        set(7, year);
        set(8, isOver);
    }
}
