/*
 * This file is generated by jOOQ.
 */
package de.jottyfan.camporganizer.db.jooq.tables;


import de.jottyfan.camporganizer.db.jooq.Camp;
import de.jottyfan.camporganizer.db.jooq.enums.EnumRole;
import de.jottyfan.camporganizer.db.jooq.tables.records.VProfileRecord;

import javax.annotation.processing.Generated;

import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Name;
import org.jooq.Record;
import org.jooq.Row7;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.TableOptions;
import org.jooq.impl.DSL;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "https://www.jooq.org",
        "jOOQ version:3.13.5"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class VProfile extends TableImpl<VProfileRecord> {

    private static final long serialVersionUID = 1959083868;

    /**
     * The reference instance of <code>camp.v_profile</code>
     */
    public static final VProfile V_PROFILE = new VProfile();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<VProfileRecord> getRecordType() {
        return VProfileRecord.class;
    }

    /**
     * The column <code>camp.v_profile.pk</code>.
     */
    public final TableField<VProfileRecord, Integer> PK = createField(DSL.name("pk"), org.jooq.impl.SQLDataType.INTEGER, this, "");

    /**
     * The column <code>camp.v_profile.forename</code>.
     */
    public final TableField<VProfileRecord, String> FORENAME = createField(DSL.name("forename"), org.jooq.impl.SQLDataType.CLOB, this, "");

    /**
     * The column <code>camp.v_profile.surname</code>.
     */
    public final TableField<VProfileRecord, String> SURNAME = createField(DSL.name("surname"), org.jooq.impl.SQLDataType.CLOB, this, "");

    /**
     * The column <code>camp.v_profile.username</code>.
     */
    public final TableField<VProfileRecord, String> USERNAME = createField(DSL.name("username"), org.jooq.impl.SQLDataType.CLOB, this, "");

    /**
     * The column <code>camp.v_profile.password</code>.
     */
    public final TableField<VProfileRecord, String> PASSWORD = createField(DSL.name("password"), org.jooq.impl.SQLDataType.CLOB, this, "");

    /**
     * The column <code>camp.v_profile.uuid</code>.
     */
    public final TableField<VProfileRecord, String> UUID = createField(DSL.name("uuid"), org.jooq.impl.SQLDataType.CLOB, this, "");

    /**
     * The column <code>camp.v_profile.roles</code>.
     */
    public final TableField<VProfileRecord, EnumRole[]> ROLES = createField(DSL.name("roles"), org.jooq.impl.SQLDataType.VARCHAR.asEnumDataType(de.jottyfan.camporganizer.db.jooq.enums.EnumRole.class).getArrayDataType(), this, "");

    /**
     * Create a <code>camp.v_profile</code> table reference
     */
    public VProfile() {
        this(DSL.name("v_profile"), null);
    }

    /**
     * Create an aliased <code>camp.v_profile</code> table reference
     */
    public VProfile(String alias) {
        this(DSL.name(alias), V_PROFILE);
    }

    /**
     * Create an aliased <code>camp.v_profile</code> table reference
     */
    public VProfile(Name alias) {
        this(alias, V_PROFILE);
    }

    private VProfile(Name alias, Table<VProfileRecord> aliased) {
        this(alias, aliased, null);
    }

    private VProfile(Name alias, Table<VProfileRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, DSL.comment(""), TableOptions.view());
    }

    public <O extends Record> VProfile(Table<O> child, ForeignKey<O, VProfileRecord> key) {
        super(child, key, V_PROFILE);
    }

    @Override
    public Schema getSchema() {
        return Camp.CAMP;
    }

    @Override
    public VProfile as(String alias) {
        return new VProfile(DSL.name(alias), this);
    }

    @Override
    public VProfile as(Name alias) {
        return new VProfile(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public VProfile rename(String name) {
        return new VProfile(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public VProfile rename(Name name) {
        return new VProfile(name, null);
    }

    // -------------------------------------------------------------------------
    // Row7 type methods
    // -------------------------------------------------------------------------

    @Override
    public Row7<Integer, String, String, String, String, String, EnumRole[]> fieldsRow() {
        return (Row7) super.fieldsRow();
    }
}
