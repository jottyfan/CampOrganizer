/*
 * This file is generated by jOOQ.
 */
package de.jottyfan.camporganizer.db.jooq;


import de.jottyfan.camporganizer.db.jooq.tables.TCamp;
import de.jottyfan.camporganizer.db.jooq.tables.TCampdocument;
import de.jottyfan.camporganizer.db.jooq.tables.TCampprofile;
import de.jottyfan.camporganizer.db.jooq.tables.TDocument;
import de.jottyfan.camporganizer.db.jooq.tables.TDocumentrole;
import de.jottyfan.camporganizer.db.jooq.tables.TLocation;
import de.jottyfan.camporganizer.db.jooq.tables.TPerson;
import de.jottyfan.camporganizer.db.jooq.tables.TPersondocument;
import de.jottyfan.camporganizer.db.jooq.tables.TProfile;
import de.jottyfan.camporganizer.db.jooq.tables.TRss;
import de.jottyfan.camporganizer.db.jooq.tables.TSales;
import de.jottyfan.camporganizer.db.jooq.tables.TSalesprofile;
import de.jottyfan.camporganizer.db.jooq.tables.records.TCampRecord;
import de.jottyfan.camporganizer.db.jooq.tables.records.TCampdocumentRecord;
import de.jottyfan.camporganizer.db.jooq.tables.records.TCampprofileRecord;
import de.jottyfan.camporganizer.db.jooq.tables.records.TDocumentRecord;
import de.jottyfan.camporganizer.db.jooq.tables.records.TDocumentroleRecord;
import de.jottyfan.camporganizer.db.jooq.tables.records.TLocationRecord;
import de.jottyfan.camporganizer.db.jooq.tables.records.TPersonRecord;
import de.jottyfan.camporganizer.db.jooq.tables.records.TPersondocumentRecord;
import de.jottyfan.camporganizer.db.jooq.tables.records.TProfileRecord;
import de.jottyfan.camporganizer.db.jooq.tables.records.TRssRecord;
import de.jottyfan.camporganizer.db.jooq.tables.records.TSalesRecord;
import de.jottyfan.camporganizer.db.jooq.tables.records.TSalesprofileRecord;

import javax.annotation.processing.Generated;

import org.jooq.Identity;
import org.jooq.impl.Internal;


/**
 * A class modelling foreign key relationships and constraints of tables of 
 * the <code>camp</code> schema.
 */
@Generated(
    value = {
        "https://www.jooq.org",
        "jOOQ version:3.13.5"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Keys {

    // -------------------------------------------------------------------------
    // IDENTITY definitions
    // -------------------------------------------------------------------------

    public static final Identity<TCampRecord, Integer> IDENTITY_T_CAMP = Identities0.IDENTITY_T_CAMP;
    public static final Identity<TCampdocumentRecord, Integer> IDENTITY_T_CAMPDOCUMENT = Identities0.IDENTITY_T_CAMPDOCUMENT;
    public static final Identity<TCampprofileRecord, Integer> IDENTITY_T_CAMPPROFILE = Identities0.IDENTITY_T_CAMPPROFILE;
    public static final Identity<TDocumentRecord, Integer> IDENTITY_T_DOCUMENT = Identities0.IDENTITY_T_DOCUMENT;
    public static final Identity<TDocumentroleRecord, Integer> IDENTITY_T_DOCUMENTROLE = Identities0.IDENTITY_T_DOCUMENTROLE;
    public static final Identity<TLocationRecord, Integer> IDENTITY_T_LOCATION = Identities0.IDENTITY_T_LOCATION;
    public static final Identity<TPersonRecord, Integer> IDENTITY_T_PERSON = Identities0.IDENTITY_T_PERSON;
    public static final Identity<TPersondocumentRecord, Integer> IDENTITY_T_PERSONDOCUMENT = Identities0.IDENTITY_T_PERSONDOCUMENT;
    public static final Identity<TProfileRecord, Integer> IDENTITY_T_PROFILE = Identities0.IDENTITY_T_PROFILE;
    public static final Identity<TRssRecord, Integer> IDENTITY_T_RSS = Identities0.IDENTITY_T_RSS;
    public static final Identity<TSalesRecord, Integer> IDENTITY_T_SALES = Identities0.IDENTITY_T_SALES;
    public static final Identity<TSalesprofileRecord, Integer> IDENTITY_T_SALESPROFILE = Identities0.IDENTITY_T_SALESPROFILE;

    // -------------------------------------------------------------------------
    // UNIQUE and PRIMARY KEY definitions
    // -------------------------------------------------------------------------


    // -------------------------------------------------------------------------
    // FOREIGN KEY definitions
    // -------------------------------------------------------------------------


    // -------------------------------------------------------------------------
    // [#1459] distribute members to avoid static initialisers > 64kb
    // -------------------------------------------------------------------------

    private static class Identities0 {
        public static Identity<TCampRecord, Integer> IDENTITY_T_CAMP = Internal.createIdentity(TCamp.T_CAMP, TCamp.T_CAMP.PK);
        public static Identity<TCampdocumentRecord, Integer> IDENTITY_T_CAMPDOCUMENT = Internal.createIdentity(TCampdocument.T_CAMPDOCUMENT, TCampdocument.T_CAMPDOCUMENT.PK);
        public static Identity<TCampprofileRecord, Integer> IDENTITY_T_CAMPPROFILE = Internal.createIdentity(TCampprofile.T_CAMPPROFILE, TCampprofile.T_CAMPPROFILE.PK);
        public static Identity<TDocumentRecord, Integer> IDENTITY_T_DOCUMENT = Internal.createIdentity(TDocument.T_DOCUMENT, TDocument.T_DOCUMENT.PK);
        public static Identity<TDocumentroleRecord, Integer> IDENTITY_T_DOCUMENTROLE = Internal.createIdentity(TDocumentrole.T_DOCUMENTROLE, TDocumentrole.T_DOCUMENTROLE.PK);
        public static Identity<TLocationRecord, Integer> IDENTITY_T_LOCATION = Internal.createIdentity(TLocation.T_LOCATION, TLocation.T_LOCATION.PK);
        public static Identity<TPersonRecord, Integer> IDENTITY_T_PERSON = Internal.createIdentity(TPerson.T_PERSON, TPerson.T_PERSON.PK);
        public static Identity<TPersondocumentRecord, Integer> IDENTITY_T_PERSONDOCUMENT = Internal.createIdentity(TPersondocument.T_PERSONDOCUMENT, TPersondocument.T_PERSONDOCUMENT.PK);
        public static Identity<TProfileRecord, Integer> IDENTITY_T_PROFILE = Internal.createIdentity(TProfile.T_PROFILE, TProfile.T_PROFILE.PK);
        public static Identity<TRssRecord, Integer> IDENTITY_T_RSS = Internal.createIdentity(TRss.T_RSS, TRss.T_RSS.PK);
        public static Identity<TSalesRecord, Integer> IDENTITY_T_SALES = Internal.createIdentity(TSales.T_SALES, TSales.T_SALES.PK);
        public static Identity<TSalesprofileRecord, Integer> IDENTITY_T_SALESPROFILE = Internal.createIdentity(TSalesprofile.T_SALESPROFILE, TSalesprofile.T_SALESPROFILE.PK);
    }
}
