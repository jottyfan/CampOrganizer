/*
 * This file is generated by jOOQ.
 */
package de.jottyfan.camporganizer.db.jooq.tables.records;


import de.jottyfan.camporganizer.db.jooq.tables.TProfile;

import java.sql.Timestamp;

import javax.annotation.processing.Generated;

import org.jooq.Field;
import org.jooq.Record7;
import org.jooq.Row7;
import org.jooq.impl.TableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "https://www.jooq.org",
        "jOOQ version:3.13.5"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class TProfileRecord extends TableRecordImpl<TProfileRecord> implements Record7<Integer, String, String, String, String, Timestamp, String> {

    private static final long serialVersionUID = 828111906;

    /**
     * Setter for <code>camp.t_profile.pk</code>.
     */
    public void setPk(Integer value) {
        set(0, value);
    }

    /**
     * Getter for <code>camp.t_profile.pk</code>.
     */
    public Integer getPk() {
        return (Integer) get(0);
    }

    /**
     * Setter for <code>camp.t_profile.forename</code>.
     */
    public void setForename(String value) {
        set(1, value);
    }

    /**
     * Getter for <code>camp.t_profile.forename</code>.
     */
    public String getForename() {
        return (String) get(1);
    }

    /**
     * Setter for <code>camp.t_profile.surname</code>.
     */
    public void setSurname(String value) {
        set(2, value);
    }

    /**
     * Getter for <code>camp.t_profile.surname</code>.
     */
    public String getSurname() {
        return (String) get(2);
    }

    /**
     * Setter for <code>camp.t_profile.username</code>.
     */
    public void setUsername(String value) {
        set(3, value);
    }

    /**
     * Getter for <code>camp.t_profile.username</code>.
     */
    public String getUsername() {
        return (String) get(3);
    }

    /**
     * Setter for <code>camp.t_profile.password</code>.
     */
    public void setPassword(String value) {
        set(4, value);
    }

    /**
     * Getter for <code>camp.t_profile.password</code>.
     */
    public String getPassword() {
        return (String) get(4);
    }

    /**
     * Setter for <code>camp.t_profile.duedate</code>.
     */
    public void setDuedate(Timestamp value) {
        set(5, value);
    }

    /**
     * Getter for <code>camp.t_profile.duedate</code>.
     */
    public Timestamp getDuedate() {
        return (Timestamp) get(5);
    }

    /**
     * Setter for <code>camp.t_profile.uuid</code>.
     */
    public void setUuid(String value) {
        set(6, value);
    }

    /**
     * Getter for <code>camp.t_profile.uuid</code>.
     */
    public String getUuid() {
        return (String) get(6);
    }

    // -------------------------------------------------------------------------
    // Record7 type implementation
    // -------------------------------------------------------------------------

    @Override
    public Row7<Integer, String, String, String, String, Timestamp, String> fieldsRow() {
        return (Row7) super.fieldsRow();
    }

    @Override
    public Row7<Integer, String, String, String, String, Timestamp, String> valuesRow() {
        return (Row7) super.valuesRow();
    }

    @Override
    public Field<Integer> field1() {
        return TProfile.T_PROFILE.PK;
    }

    @Override
    public Field<String> field2() {
        return TProfile.T_PROFILE.FORENAME;
    }

    @Override
    public Field<String> field3() {
        return TProfile.T_PROFILE.SURNAME;
    }

    @Override
    public Field<String> field4() {
        return TProfile.T_PROFILE.USERNAME;
    }

    @Override
    public Field<String> field5() {
        return TProfile.T_PROFILE.PASSWORD;
    }

    @Override
    public Field<Timestamp> field6() {
        return TProfile.T_PROFILE.DUEDATE;
    }

    @Override
    public Field<String> field7() {
        return TProfile.T_PROFILE.UUID;
    }

    @Override
    public Integer component1() {
        return getPk();
    }

    @Override
    public String component2() {
        return getForename();
    }

    @Override
    public String component3() {
        return getSurname();
    }

    @Override
    public String component4() {
        return getUsername();
    }

    @Override
    public String component5() {
        return getPassword();
    }

    @Override
    public Timestamp component6() {
        return getDuedate();
    }

    @Override
    public String component7() {
        return getUuid();
    }

    @Override
    public Integer value1() {
        return getPk();
    }

    @Override
    public String value2() {
        return getForename();
    }

    @Override
    public String value3() {
        return getSurname();
    }

    @Override
    public String value4() {
        return getUsername();
    }

    @Override
    public String value5() {
        return getPassword();
    }

    @Override
    public Timestamp value6() {
        return getDuedate();
    }

    @Override
    public String value7() {
        return getUuid();
    }

    @Override
    public TProfileRecord value1(Integer value) {
        setPk(value);
        return this;
    }

    @Override
    public TProfileRecord value2(String value) {
        setForename(value);
        return this;
    }

    @Override
    public TProfileRecord value3(String value) {
        setSurname(value);
        return this;
    }

    @Override
    public TProfileRecord value4(String value) {
        setUsername(value);
        return this;
    }

    @Override
    public TProfileRecord value5(String value) {
        setPassword(value);
        return this;
    }

    @Override
    public TProfileRecord value6(Timestamp value) {
        setDuedate(value);
        return this;
    }

    @Override
    public TProfileRecord value7(String value) {
        setUuid(value);
        return this;
    }

    @Override
    public TProfileRecord values(Integer value1, String value2, String value3, String value4, String value5, Timestamp value6, String value7) {
        value1(value1);
        value2(value2);
        value3(value3);
        value4(value4);
        value5(value5);
        value6(value6);
        value7(value7);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached TProfileRecord
     */
    public TProfileRecord() {
        super(TProfile.T_PROFILE);
    }

    /**
     * Create a detached, initialised TProfileRecord
     */
    public TProfileRecord(Integer pk, String forename, String surname, String username, String password, Timestamp duedate, String uuid) {
        super(TProfile.T_PROFILE);

        set(0, pk);
        set(1, forename);
        set(2, surname);
        set(3, username);
        set(4, password);
        set(5, duedate);
        set(6, uuid);
    }
}
