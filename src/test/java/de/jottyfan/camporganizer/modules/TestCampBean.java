package de.jottyfan.camporganizer.modules;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.sql.Date;

import org.junit.jupiter.api.Test;

import de.jottyfan.camporganizer.CampBean;

/**
 *
 * @author jotty
 *
 */
public class TestCampBean {

	@Test
	public void testGetCampTime() {
		CampBean bean = new CampBean();
		bean.setArrive(Date.valueOf("2020-12-24"));
		bean.setDepart(Date.valueOf("2022-12-24"));
		assertEquals("Do. 24.12. - Sa. 24.12.2022", bean.getCampTime());
	}
}
